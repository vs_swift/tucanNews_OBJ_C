//
//  DetailsController.h
//  tucanNews(OBJ)
//
//  Created by Private on 1/28/18.
//  Copyright © 2018 Private. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsController : UIViewController

@property (nonatomic) NSString *indexString;
@property (nonatomic) NSString *articleString;

@end

