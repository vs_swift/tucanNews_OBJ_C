//
//  ListController.m
//  tucanNews(OBJ)
//
//  Created by Private on 1/28/18.
//  Copyright © 2018 Private. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ListController.h"
#import "ListCell.h"
#import "DetailsController.h"

@interface ListController ()

@property (nonatomic) NSArray *myStrings;

@end

@implementation ListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myStrings = [NSArray arrayWithObjects: @"The Coca-Cola Company is a beverage company. The Company owns or licenses and markets non-alcoholic beverage brands, primarily sparkling beverages and a range of still beverages, such as waters, flavored waters and enhanced waters, juices and juice drinks, ready-to-drink teas and coffees, sports drinks, dairy and energy drinks. The Company’s segments include Europe, Middle East and Africa; Latin America; North America; Asia Pacific; Bottling Investments, and Corporate. The Company owns and markets a range of non-alcoholic sparkling beverage brands, including Coca-Cola, Diet Coke, Fanta and Sprite. The Company owns or licenses and markets over 500 non-alcoholic beverage brands. The Company markets, manufactures and sells beverage concentrates, which are referred to as beverage bases, and syrups, including fountain syrups, and finished sparkling and still beverages", @"Long Blockchain Corp., formerly Long Island Iced Tea Corp., is a holding company operating through its subsidiary, Long Island Brand Beverages, LLC (LIBB). The Company is engaged in the production and distribution of Non-Alcoholic Ready-to-Drink (NARTD) iced tea in the beverage industry. It is organized around its brand, Long Island Iced Tea. Long Island Iced Tea is sold primarily on the East Coast of the United States through a network of national and regional retail chains and distributors. The Company produces brewed tea, using black tea leaves, purified water and natural cane sugar or sucralose. The Company’s Long Island Iced Tea’s flavors include lemon, peach, raspberry, guava, mango, diet lemon, diet peach and sweet tea. It also offers lower calorie iced tea in over 12 ounce bottles. The lower calorie flavor options include mango, raspberry and peach", @"Investors sentiment decreased to 0.85 in Q3 2017. Its down 0.15, from 1 in 2017Q2. It worsened, as 55 investor", @"General Motors is in a race to be the first company to mass-produce self-driving cars. But a recent crash with a San Francisco motorcyclist has illustrated the challenge of assigning blame when an autonomous vehicle gets in an accident", nil];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.myStrings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListCell *listCell = [tableView dequeueReusableCellWithIdentifier:@"ListCell" forIndexPath:indexPath];
    
    [listCell setData:[NSString stringWithFormat:@"%ld", (long)indexPath.row] article:self.myStrings[indexPath.row]];
    
    return listCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *someArray = [NSArray arrayWithObjects: [NSString stringWithFormat:@"Article: %ld", indexPath.row], self.myStrings[indexPath.row], nil];
    
    [self performSegueWithIdentifier:@"ShowDetails" sender:someArray];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowDetails"]) {
        DetailsController *detailsController = [segue destinationViewController];
        detailsController.indexString = (NSString *)sender[0];
        detailsController.articleString = (NSString *)sender[1];
    }
}

@end
