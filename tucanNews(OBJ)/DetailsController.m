//
//  DetailsController.m
//  tucanNews(OBJ)
//
//  Created by Private on 1/28/18.
//  Copyright © 2018 Private. All rights reserved.
//

#import "DetailsController.h"

@interface DetailsController ()

@property (weak, nonatomic) IBOutlet UILabel *indexLabel;
@property (weak, nonatomic) IBOutlet UITextView *articleTextView;

@end

@implementation DetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.indexLabel.text = self.indexString;
    self.articleTextView.text = self.articleString;
}

@end


