//
//  ListCell.m
//  tucanNews(OBJ)
//
//  Created by Private on 1/28/18.
//  Copyright © 2018 Private. All rights reserved.
//

#import "ListCell.h"

@implementation ListCell

- (void)setData: (NSString *)index article: (NSString *)article {
    
    self.indexLabel.text = index;
    self.articleLabel.text = article;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
