//
//  AppDelegate.h
//  tucanNews(OBJ)
//
//  Created by Private on 1/28/18.
//  Copyright © 2018 Private. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

